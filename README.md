# Package List Application

This is a web application for viewing Debian dpkg status files.

[Backend](https://gitlab.com/Samsai/package-list-service)
[Frontend](https://gitlab.com/Samsai/package-list-frontend)

**Note:** This repository only contains the docker-compose.yml file for deploying
the application. The code lives in separate repositories.